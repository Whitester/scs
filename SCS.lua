/*----------------------------------------------------------------------

    Copyright (C) 2014  White_star

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
The "Steam addon collection" command is an example on how you could use it.

----------------------------------------------------------------------*/

-- Steam addon collection
 
local function openContent( ply, text, team) -- the function itself
    if string.sub( text, 1, 8 ) == "!content" then -- the check for the text, replace !content with your command
        ply:SendLua([[ gui.OpenURL("[CENSORED]") ]]) -- An Url to open, you can replace this line with any function.
        print(ply .. "Wrote !content") -- Optional: Writes to console which player wrote it(good for logging)
end
end
hook.Add("PlayerSay", "openContent", openContent) -- Hooking the function in the PlayerSay System
